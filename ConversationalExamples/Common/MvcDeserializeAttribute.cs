﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConversationalExamples.Common
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public sealed class MvcDeserializeAttribute : CustomModelBinderAttribute
    {
        public MvcDeserializeAttribute()
        {
        }

        internal MvcSerializer Serializer { get; set; }

        public override IModelBinder GetBinder()
        {
            return new DeserializingModelBinder(Serializer);
        }

        private sealed class DeserializingModelBinder : IModelBinder
        {
            private readonly MvcSerializer _serializer;

            public DeserializingModelBinder(MvcSerializer serializer)
            {
                _serializer = serializer ?? new MvcSerializer();
            }

            [SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.Web.Mvc.ValueProviderResult.ConvertTo(System.Type)", Justification = "The target object should make the correct culture determination, not this method.")]
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                if (bindingContext == null)
                {
                    throw new ArgumentNullException("bindingContext");
                }

                ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                if (valueProviderResult == null)
                {
                    // nothing found
                    return null;
                }

                string serializedValue = (string)valueProviderResult.ConvertTo(typeof(string));
                return _serializer.Deserialize(serializedValue);
            }
        }
    }
}