﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ConversationalExamples.Common
{
    internal interface IMachineKey
    {
        byte[] Unprotect(string protectedData, params string[] purposes);
        string Protect(byte[] userData, params string[] purposes);
    }

    // Concrete implementation of IMachineKey that talks to the static MachineKey 
    internal sealed class MachineKeyWrapper : IMachineKey
    {
        private static readonly MachineKeyWrapper _singletonInstance = new MachineKeyWrapper();

        public static MachineKeyWrapper Instance
        {
            get
            {
                return _singletonInstance;
            }
        }

        public byte[] Unprotect(string protectedData, params string[] purposes)
        {
            byte[] protectedBytes = Convert.FromBase64String(protectedData);
            return MachineKey.Unprotect(protectedBytes, purposes);
        }

        public string Protect(byte[] userData, params string[] purposes)
        {
            byte[] protectedBytes = MachineKey.Protect(userData, purposes);
            return Convert.ToBase64String(protectedBytes);
        }
    }
}