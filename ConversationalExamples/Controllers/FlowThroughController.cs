﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConversationalExamples.Common;
using ConversationalExamples.Models;

namespace ConversationalExamples.Controllers
{
    public class FlowThroughController : Controller
    {
        /*
         * This flow through demonstrates the ability to take keep state 
         * without the use of sessions.
         */

        // GET: FlowThrough
        [HttpGet]
        public ActionResult Index()
        {
            // create a basket of zeroes for demo purposes
            var basket = new Basket()
            {
                ItemA = "0",
                ItemB = "0",
                ItemC = "0"
            };

            return View(basket);
        }

        [HttpPost]
        public ActionResult Index(string userInput, [MvcDeserialize] Basket basket)
        {
            basket.ItemA = userInput;

            return RedirectToAction("Step2", basket);
        }

        [HttpGet]
        public ActionResult Step2(Basket basket)
        {
            return View(basket);
        }

        [HttpPost]
        public ActionResult Step2(string userInput, [MvcDeserialize] Basket basket)
        {
            basket.ItemB = userInput;

            return RedirectToAction("Step3", basket);
        }

        [HttpGet]
        public ActionResult Step3(Basket basket)
        {
            return View(basket);
        }
    }
}