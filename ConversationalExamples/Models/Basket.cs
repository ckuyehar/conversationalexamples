﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConversationalExamples.Models
{
    public class Basket
    {
        public string ItemA { get; set; }
        public string ItemB { get; set; }
        public string ItemC { get; set; }
    }
}